﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
[ExecuteInEditMode]
public class Road : MonoBehaviour
{
		public List<Node>
				nodes;
		public List<Trail>
				trails;
		public Material
				roadMaterial;

		public void DragLastNode (Vector3 pos)
		{
				nodes [nodes.Count - 1].gameObject.transform.position = pos;
		}
		
		
		
		public void AddNode (Vector3 pos)
		{
#if UNITY_EDITOR
				GameObject newPoint = new GameObject();

				Node newNode = newPoint.AddComponent<Node>();
				newNode.Road = this.gameObject.GetComponent<Road>();	
				nodes.Add (newNode);
			
				newPoint.name = newNode.Road.name + " Node "+nodes.Count.ToString();
				newPoint.transform.position = pos;
				newPoint.transform.parent = this.gameObject.transform;			
#endif
		}

		public void RemoveNode (int index)
		{
				#if UNITY_EDITOR
				DestroyImmediate(nodes[index].gameObject);
				nodes.RemoveAt(index);
				#endif
		}
	
		public void RemoveNodes ()
		{
#if UNITY_EDITOR
				for (int i=0; i<nodes.Count; i++)
				{
					DestroyImmediate(nodes[i].gameObject);
				}
				nodes.Clear();
#endif
		}
	
		public void AddTrail (Node startPoint)
		{
#if UNITY_EDITOR
				GameObject newPath = new GameObject();
				newPath.tag = "Trail";
				MeshFilter filter = newPath.AddComponent<MeshFilter>();
				filter.mesh = RoadCreator.InstantiateQuadMesh();
				newPath.AddComponent<MeshCollider>();
				MeshRenderer renderer = newPath.AddComponent<MeshRenderer>();
				//renderer.material = roadMaterial;
				
				Trail newTrail = newPath.AddComponent<Trail>();
				newTrail.Road = this.gameObject.GetComponent<Road>();
				newTrail.Filter = filter;
				newTrail.StartNode = startPoint;
				newTrail.EndNode = nodes [nodes.Count - 1].GetComponent<Node>();
				trails.Add(newTrail);
				
				newPath.name = newTrail.Road.name +" Trail " +trails.Count.ToString();
				newPath.transform.parent = this.gameObject.transform;
#endif
		}

		public void RemoveTrails ()
		{
#if UNITY_EDITOR
				for (int i=0; i<trails.Count; i++)
				{
					DestroyImmediate(trails[i].gameObject);
				}
				trails.Clear();
#endif
		}
		

		public void LandTrailBlocks()
		{
#if UNITY_EDITOR
				for (int i=0; i<trails.Count; i++)
				{
						Vector3 startPoint = trails[i].StartNode.transform.position;

						int test = 0;

						if(i==trails.Count-1)
						{
								test = Mathf.CeilToInt (trails[i].transform.localScale.z);
						}
						else
						{
								if(trails[i].transform.forward.x>trails[i].transform.forward.z)
								{
										if(Mathf.Abs (trails[i].transform.forward.x - trails[i+1].transform.forward.x) > 0.2f)
										{
												test = Mathf.FloorToInt (trails[i].transform.localScale.z);
												startPoint += trails[i].transform.forward/4;
										}
										else
												test = Mathf.CeilToInt (trails[i].transform.localScale.z);
								}
								else
								{
										if(Mathf.Abs (trails[i].transform.forward.z - trails[i+1].transform.forward.z) > 0.2f)
										{
												test = Mathf.FloorToInt (trails[i].transform.localScale.z);
												startPoint += trails[i].transform.forward/4;
										}
										else
												test = Mathf.CeilToInt (trails[i].transform.localScale.z);
								}
						}
						transform.localScale = Vector3.one;
						DestroyImmediate(trails[i].Filter);
						DestroyImmediate(trails[i].GetComponent<MeshFilter>());
						DestroyImmediate(trails[i].GetComponent<MeshRenderer>());
						for(int k=0; k<test; k++)
						{
								GameObject plate = (GameObject)PrefabUtility.InstantiatePrefab(RoadCreator.Instance.roadPrefab);
								plate.transform.position =  startPoint + trails[i].transform.forward*k;
								plate.transform.forward = trails[i].transform.forward;
								plate.transform.parent = trails[i].transform;
							
						}
				}
#endif
		}


		public void PaintTrailOnTerrain()
		{
#if UNITY_EDITOR				
				for (int i=0; i<trails.Count; i++)
				{
						float nodeDelta = (trails[i].EndNode.transform.position - trails[i].StartNode.transform.position).magnitude;
						Vector3 point = trails[i].transform.position - trails[i].transform.forward * (nodeDelta / 2);
						
						for (int k=0; k<=Mathf.CeilToInt(nodeDelta); k++) {
							
								Vector3 current = RoadCreator.Instance.WorldToAlphaMapTerrain (point + trails[i].transform.forward * k);
								int curX = (int)current.x;
								int curY = (int)current.z;
								
								for (int a=-1; a<1; a++) {
										for (int b=-1; b<1; b++) {
											
												RoadCreator.Instance.alphaMask [curX + a, curY + b] = 3;
												RoadCreator.Instance.Alpha [curX + a, curY + b, RoadCreator.Instance.borderSplat] = Mathf.Lerp (RoadCreator.Instance.Alpha [curX + a, curY + b, RoadCreator.Instance.borderSplat], 1f, RoadCreator.Instance.brush[a+1,b+1]);
												RoadCreator.Instance.Alpha [curX + a, curY + b, 0] = 1 - RoadCreator.Instance.Alpha [curX + a, curY + b, RoadCreator.Instance.borderSplat];
//												RoadCreator.Instance.Alpha [curX + a, curY + b, 1] = 1f;
//												RoadCreator.Instance.Alpha [curX + a, curY + b, 0] = 0f;
						
												for(int j=1; j<RoadCreator.Instance.terrain.terrainData.alphamapLayers;j++){
														if(j!= RoadCreator.Instance.borderSplat)
														{
																RoadCreator.Instance.Alpha [curX + a, curY + b, j] = 0f;
														}
												}
										}
								}
						}
				}
#endif
		}

		public void BuildTrailOnTerrain()
		{
#if UNITY_EDITOR
				for(int i=0; i<trails.Count; i++)
				{
						float delta = 0.01f / RoadCreator.Instance.terrain.terrainData.heightmapScale.y;
						float nodeDelta = (trails[i].EndNode.transform.position - trails[i].StartNode.transform.position).magnitude;
						Vector3 point = trails[i].transform.position - trails[i].transform.forward * (nodeDelta / 2);
						for (int k=0; k<=Mathf.CeilToInt(nodeDelta); k++) {
								Vector3 current = RoadCreator.Instance.WorldToHeightMapTerrain(point+trails[i].transform.forward*k);
								int curX = (int)current.x;
								int curY = (int)current.z;
								float h = current.y / RoadCreator.Instance.terrain.terrainData.heightmapScale.y ;
								
								for (int a=-1; a<1; a++) {
										for (int b=-1; b<1; b++) {										
											RoadCreator.Instance.heightMask[curX + a, curY + b] = 3;
											RoadCreator.Instance.Heights [curX + a, curY + b] = Mathf.Lerp (RoadCreator.Instance.Heights [curX + a, curY + b], 
											                                                                h - delta,
						                                                                					RoadCreator.Instance.brush[a+1,b+1]);
//											RoadCreator.Instance.Heights [curX + a, curY + b] = h - delta;
										}
								}
						}
				}
#endif
		}
		/*void OnDrawGizmos ()
		{
				Gizmos.color = Color.blue;
				Gizmos.DrawSphere (transform.position + Vector3.up * 10, 5f);
		}*/
}
