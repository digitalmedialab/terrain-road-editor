﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

[System.Serializable]
[ExecuteInEditMode]
public class RoadCreator : MonoBehaviour
{		
		public GameObject roadPrefab;
		public List<Road>
				roads;
		public int borderSplat;
		public int[] startupUV = {0,1,0,1,0,0,1,1};
		public float[,] brush = {{0.5f, 0.6f, 0.5f},
									{0.7f, 0.9f, 0.7f},
									{0.5f, 0.6f, 0.5f}};
		public Terrain terrain;
		//
		//
		//
		//[SerializeField]
		private float[,]
				_heights;
		//[SerializeField]
		private float[,,]
				_alpha;
		//[SerializeField]
		public int[,]
				heightMask;
		//[SerializeField]
		public int[,]
				alphaMask;
		public List<Vector2> heightEdgers;
		public List<Vector2> alphaEdgers;
		//
		//
		//
		//[Range(0f,1f)]
		//public float
		//filter;
		[Range(0f, 1f)]
		public float
				filterLimit;
		[Range(0f,1f)]
		public float
				alphaFilter;
		[Range(0f,0.2f)]
		public float
				filterStep;
		[Range(0f, 1f)]
		public float
				alphaFilterLimit;
		[Range(0f,0.5f)]
		public float
				alphaFilterStep;
		[Range(0f,1f)]
		public float
				smoothFilter;
		[Range(1,10)]
		public int
				smoothIterations;
		[Range(1,10)]
		public int
				alphaSmoothIterations;
		[Range(0f, 60f)]
		public float
				interval;
		
		RoadCreator ()
		{
				roads = new List<Road> ();
		}
	
	#region Singleton 
		private static RoadCreator _instance;
		
		public static RoadCreator Instance {
				get {
						if (_instance == null) {
								_instance = GameObject.FindObjectOfType<RoadCreator> ();
								_instance.terrain = GameObject.Find ("Terrain").GetComponent<Terrain> ();
								_instance._heights = _instance.terrain.terrainData.GetHeights (0, 0,
							                                                               		_instance.terrain.terrainData.heightmapResolution,
							                                                               		_instance.terrain.terrainData.heightmapResolution);
								_instance.heightMask = new int[_instance.terrain.terrainData.heightmapResolution,
				                               					_instance.terrain.terrainData.heightmapResolution];
								_instance._alpha = _instance.terrain.terrainData.GetAlphamaps (0, 0,
							                                                            		_instance.terrain.terrainData.alphamapResolution,
							                                                            		_instance.terrain.terrainData.alphamapResolution);
								_instance.alphaMask = new int[_instance.terrain.terrainData.alphamapResolution,
				                              					_instance.terrain.terrainData.alphamapResolution];
						}
						return _instance;
				}
		}
		#endregion

		public float StepX {
				get {
						return terrain.terrainData.heightmapScale.x;
				}
		}
		
		public float StepY {
				get {
						return terrain.terrainData.heightmapScale.y;
				}
		}

		public float StepZ {
				get {
						return terrain.terrainData.heightmapScale.z;
				}
		}

		public float[,] Heights {
				get { 
						return _heights;
				}
				set { 
						_heights = value;
				}
		}

		public float[,,] Alpha {
				get { 
						return _alpha;
				}
				set { 
						_alpha = value;
				}
		}
	
		public Vector3 WorldToHeightMapTerrain (Vector3 pos)
		{
				// Subtract terrain position from object position
				Vector3 coord = pos - terrain.gameObject.transform.position;
		
				// Divide x and z coordinates by terrain's sizes
				coord.x = coord.x / terrain.terrainData.size.x;
				coord.z = coord.z / terrain.terrainData.size.z;
				
				return new Vector3 (Mathf.RoundToInt (coord.z * terrain.terrainData.heightmapResolution), pos.y, Mathf.RoundToInt (coord.x * terrain.terrainData.heightmapResolution));
		}

		public Vector3 WorldToAlphaMapTerrain (Vector3 pos)
		{
				Vector3 coord = pos - terrain.gameObject.transform.position;

				coord.x = coord.x / terrain.terrainData.size.x;
				coord.z = coord.z / terrain.terrainData.size.z;

				return new Vector3 (Mathf.RoundToInt (coord.z * terrain.terrainData.alphamapResolution), coord.y, Mathf.RoundToInt (coord.x * terrain.terrainData.alphamapResolution));
		}

		public Vector3[] GetNormalizedCoordinates (Vector3[] vertex, int divider)
		{
				Vector3[] result = new Vector3[4];
				for (int i=0; i<result.Length; i++) {
						result [i] = vertex [i] - terrain.gameObject.transform.position;
						result [i] = new Vector3 (result [i].z / terrain.terrainData.size.z * divider, 
			                          (result [i].y) / terrain.terrainData.size.y, 
			                          result [i].x / terrain.terrainData.size.x * divider);
				}
		
				bool[] toHigh = new bool[4];
				bool[] toRight = new bool[4];
		
				if (result [0].z > result [2].z) {
						toHigh [0] = true;
						toHigh [2] = false;
				} else {
						toHigh [0] = false;
						toHigh [2] = true;
				}
		
				if (result [1].z > result [3].z) {
						toHigh [1] = true;
						toHigh [3] = false;
				} else {
						toHigh [1] = false;
						toHigh [3] = true;
				}
				if (result [1].x > result [3].x) {
						toRight [1] = true;
						toRight [3] = false;
				} else {
						toRight [1] = false;
						toRight [3] = true;
				}
		
				if (toHigh [0] == toHigh [1]) {
						toRight [0] = !toRight [1];
						toRight [2] = !toRight [3];
				} else {
						toRight [0] = toRight [1];
						toRight [2] = toRight [3];
				}
		
				for (int i=0; i<result.Length; i++) {
						result [i] = new Vector3 (toRight [i] ? Mathf.CeilToInt (result [i].x) : Mathf.FloorToInt (result [i].x),
						                          result [i].y,
						                          toHigh [i] ? Mathf.CeilToInt (result [i].z) : Mathf.FloorToInt (result [i].z));
				}
		
		
				return result; 
		}
	
		public void SetHeights (Vector3 point, float[,] heights)
		{
				Vector3 pos = WorldToHeightMapTerrain (point);
				terrain.terrainData.SetHeights ((int)pos.x, (int)pos.z, heights);
		}

		public void AddRoad (Vector3 target)
		{
#if UNITY_EDITOR
				GameObject newRoad = new GameObject();
				newRoad.name = "Road "+roads.Count.ToString();
				newRoad.transform.position = target;

				Road roadScript = newRoad.AddComponent<Road>();
				roadScript.nodes = new List<Node> ();
				roadScript.trails = new List<Trail> ();
				roadScript.AddNode(target);
				roads.Add(roadScript);
#endif
		}
		
		public void RemoveRoad (int index)
		{
#if UNITY_EDITOR
				DestroyImmediate (roads [index].gameObject);
				roads.RemoveAt (index);
#endif
		}

		public void ClearRoads ()
		{
#if UNITY_EDITOR
				for (int i = 0; i<roads.Count; i++)
				{
					DestroyImmediate(roads[i].gameObject);
				}
				roads.Clear();
#endif
		}

		public static Mesh InstantiateQuadMesh ()
		{
				Mesh result = new Mesh ();

				Vector3 [] vertex = new Vector3[4];
				vertex [0] = new Vector3 (-0.5f, 0, -0.5f);
				vertex [1] = new Vector3 (0.5f, 0, -0.5f);
				vertex [2] = new Vector3 (0.5f, 0, 0.5f);
				vertex [3] = new Vector3 (-0.5f, 0, 0.5f);
				
		
				result.vertices = vertex;
		
				int [] tris = new int[6];
		
				tris [0] = 0;
				tris [1] = 3;
				tris [2] = 1;
				tris [3] = 3;
				tris [4] = 2;
				tris [5] = 1;
		
				result.triangles = tris;
		
				Vector3[] normals = new Vector3[4];
				normals [0] = Vector3.up;
				normals [1] = Vector3.up;
				normals [2] = Vector3.up;
				normals [3] = Vector3.up;
		
				result.normals = normals;
		
				Vector2 [] uvs = new Vector2[4];
				uvs [0] = new Vector2 (0, 0);
				uvs [1] = new Vector2 (1, 0);
				uvs [2] = new Vector2 (0, 1);
				uvs [3] = new Vector2 (1, 1);
		
				result.uv = uvs;

				return result;
		}

		public void SmoothHeightsAround ()
		{

				List<Vector2> heightEdgers = new List<Vector2> ();
				
				for (int i=0; i<_heights.GetLength(0); i++) {
						for (int j=0; j<_heights.GetLength(0); j++) {
					
								if (heightMask [i, j] == 3) {
										for (int a=-1; a<2; a++) {
												for (int b=-1; b<2; b++) {
														try {
																if (heightMask [i + a, j + b] != 3) {
																		heightMask [i + a, j + b] = 1;
																		_heights [i + a, j + b] = Mathf.Lerp (_heights [i + a, j + b], _heights [i, j], 0.75f);
																}
														} catch {
														}
												}
										}
								}
					
						}
				}

				for (int i=1; i<_heights.GetLength(0)-1; i++) {
						for (int j=1; j<_heights.GetLength (1)-1; j++) {

								if (heightMask [i, j] != 0) {
										float sum = 0f;
										for (int a=-1; a<2; a++) {
												for (int b=-1; b<2; b++) {
														sum += _heights [i + a, j + b];
												}
										}
										sum /= 9;
										_heights [i, j] = Mathf.Lerp (_heights [i, j], sum, smoothFilter);
								}
						}
				}
				terrain.terrainData.SetHeights (0, 0, _heights);
				Debug.Log ("Height Mask initialized");
		}

		public void SmoothAlphaAround ()
		{
				List<Vector2> alphaEdgers = new List<Vector2> ();
		
				alphaFilter = 1f;
		
				for (int i=0; i<_alpha.GetLength (0); i++) {
						for (int j=0; j<_alpha.GetLength (1); j++) {
				
								if (alphaMask [i, j] == 3) {
										for (int a =-1; a<2; a++) {
												for (int b=-1; b<2; b++) {

														try {
																if (alphaMask [i + a, j + b] != 3) {
																		//_alpha [i + a, j + b, 0] = 0f;
																		//_alpha [i + a, j + b, 1] = Mathf.Lerp (_alpha [i + a, j + b, 1], _alpha [i, j, 1], 0.5f);
																		alphaMask [i + a, j + b] = 1;
																}
														} catch {
														}
							
												}
										}
								}							
						}
				}
		
				for (int i=0; i<_alpha.GetLength (0); i++) {
						for (int j=0; j<_alpha.GetLength (1); j++) {
				
								if (alphaMask [i, j] == 1) {
										alphaEdgers.Add (new Vector2 (i, j));
								}				
						}
				}

				for (int k=0; k<1; k++) {
						//AlphaSmooth ();
						for (int i=1; i<_alpha.GetLength (0)-1; i++) {
								for (int j=1; j<_alpha.GetLength (1) - 1; j++) {
										if (alphaMask [i, j] == 1) {
												float sum = 0f;
												for (int a=-1; a<2; a++) {
														for (int b=-1; b<2; b++) {
																sum += _alpha [i + a, j + b, 1];
														}
												}
												sum /= 9;

//												_alpha [i, j, 1] = Mathf.Lerp (_alpha [i, j, 1], sum, smoothFilter);
//												_alpha [i, j, 0] = 1 - _alpha [i, j, 1];
//												for(int l=2; l<terrain.terrainData.alphamapLayers; l++)
//												{
//													_alpha [i, j, l] = 0f;
//												}
												_alpha [i, j, borderSplat] = Mathf.Lerp (_alpha [i, j, borderSplat], sum, smoothFilter);
												_alpha [i, j, 0] = 1 - _alpha [i, j, borderSplat];
												for (int l=1; l<terrain.terrainData.alphamapLayers; l++) {
														if (l != borderSplat) {
																_alpha [i, j, l] = 0f;
														}
												}

										}
								}
						}
				}
				terrain.terrainData.SetAlphamaps (0, 0, _alpha);
				//Debug.Log ("Alpha Mask Initialized");
		}

		public void BakeTextureFromHeightMask (string fileName, int[,] mask)
		{
				Texture2D texture = new Texture2D (mask.GetLength (0), mask.GetLength (1));
				for (int i=0; i<mask.GetLength (0); i++) {
						for (int j=0; j<mask.GetLength (1); j++) {
								Color col = new Color ();
								if (mask [i, j] == 3) {
										col = Color.red;
								} else if (mask [i, j] == 2) {
										col = Color.blue;
								} else if (mask [i, j] == 1) {
										col = Color.green;
								} else if (mask [i, j] == 4) {
										col = Color.yellow;
								} else {
										col = Color.black;
								}
				
								texture.SetPixel (j, i, col);
						}
				}
				texture.Apply ();
				File.WriteAllBytes (Application.dataPath + "/" + fileName + ".png", texture.EncodeToPNG ());
		
		}

		public void MakeRoads ()
		{
				RoadCreator.Instance.Heights = RoadCreator.Instance.terrain.terrainData.GetHeights (0, 0,
				                                                                                    RoadCreator.Instance.terrain.terrainData.heightmapResolution,
				                                                                                    RoadCreator.Instance.terrain.terrainData.heightmapResolution);
				for (int i=0; i<roads.Count; i++) {
						roads [i].BuildTrailOnTerrain ();
				}
				terrain.terrainData.SetHeights (0, 0, _heights);
		}

		public void PaintRoads ()
		{
				RoadCreator.Instance.Alpha = RoadCreator.Instance.terrain.terrainData.GetAlphamaps (0, 0,
				                                                                                    RoadCreator.Instance.terrain.terrainData.alphamapResolution,
				                                                                                    RoadCreator.Instance.terrain.terrainData.alphamapResolution);
				for (int i=0; i<roads.Count; i++) {
						roads [i].PaintTrailOnTerrain ();
				}
				terrain.terrainData.SetAlphamaps (0, 0, _alpha);
		}

		public void PlateRoads ()
		{
				for (int i=0; i<roads.Count; i++) {
						roads [i].LandTrailBlocks ();
				}
		}
}
