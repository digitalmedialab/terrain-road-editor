﻿using UnityEngine;
using System.Collections;

[System.Serializable]
[ExecuteInEditMode]
public class Node : MonoBehaviour
{	
		public DelegateController.NodeMovement NodeMovement;
		public Road _road;
		[SerializeField]
		private Trail
				_StartNode;
		[SerializeField]	
		private Trail
				_EndNode;
		
		public Road Road {
				get {
						return _road;
				}
				set {
						_road = value;
				}
		}

//		public Trail IsStartFor {
//				get {
//						return _StartNode;
//				}
//				set {
//						_StartNode = value;
//				}
//		}
//
//		public Trail IsEndFor {
//				get {
//						return _EndNode;
//				}
//				set {
//						_EndNode = value;
//				}
//		}

		void OnEnable ()
		{
				gameObject.transform.hasChanged = false;
		}
	
//		public void OnMovement ()
//		{
//#if UNITY_EDITOR
// 				if(_StartNode != null & _EndNode != null)
//				{
//					transform.position = Vector3.Lerp(_StartNode.transform.position - 
//					                                  _StartNode.transform.forward * _StartNode.transform.localScale.z / 2,
//					                                  _EndNode.transform.position + 
//					                                  _EndNode.transform.forward * _EndNode.transform.localScale.z / 2, 0.5f);
//				}
//				else if(_StartNode == null & _EndNode != null)
//				{
//					transform.position = _EndNode.transform.position + _EndNode.transform.forward * _EndNode.transform.localScale.z / 2;
//				}
//				else if(_StartNode != null & _EndNode == null)
//				{
//					transform.position = _StartNode.transform.position - _StartNode.transform.forward * _StartNode.transform.localScale.z / 2;
//				}
//				transform.hasChanged = false;
//#endif
//		}
		
		void OnDrawGizmos ()
		{
				Gizmos.color = Color.green;
				//Gizmos.DrawCube (transform.position + Vector3.up, Vector3.one + Vector3.up * 2);
				Gizmos.DrawSphere (transform.position, 1);
		}

		void Update ()
		{
#if UNITY_EDITOR
				if (gameObject.transform.hasChanged) {
						if(NodeMovement != null){
								NodeMovement ();
						}
						//RoadCreator.Instance.ChangeHeightOnPoint(RoadCreator.Instance.WorldToTerrain(transform.position));
						gameObject.transform.hasChanged = false;	
				}
#endif
		}		
}
