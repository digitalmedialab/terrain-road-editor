﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Node))]
public class NodeEditor : Editor
{
		Node script;
		bool isEdit;

		void OnSceneGUI ()
		{
				Event e = Event.current;
				int controlID = GUIUtility.GetControlID (FocusType.Native);
				EventType type = e.GetTypeForControl (controlID);

				if (type == EventType.KeyDown) {
						switch (e.keyCode) {
						case KeyCode.Space:
								if (!isEdit) {
										GUIUtility.hotControl = controlID;
										isEdit = true;
								} else {
										GUIUtility.hotControl = 0;
										isEdit = false;
								}
								break;
						}
						e.Use ();
				}
			
				if (isEdit) {
						if (type == EventType.MouseDown && e.button == 0) {							

								GUIUtility.hotControl = controlID;

								Ray ray = HandleUtility.GUIPointToWorldRay (e.mousePosition);
								RaycastHit hit;
								if (Physics.Raycast (ray, out hit)) {
										//RoadCreator.Instance.AddNodeToPath (hit.point);
										//RoadCreator.Instance.AddTrailToCertain (script.gameObject);
										script.Road.AddNode(hit.point);
										script.Road.AddTrail(script);
										//Debug.Log ("Fuck!");
								}								
								e.Use ();
						}

						if (type == EventType.MouseDrag && e.button == 0) {
							
								GUIUtility.hotControl = controlID;							
							
								Ray ray = HandleUtility.GUIPointToWorldRay (e.mousePosition);
								RaycastHit hit;
								if (Physics.Raycast (ray, out hit)) {
										// Move last node in path while moving mouse with LMB down
										script.Road.DragLastNode(hit.point);
								}
								e.Use ();
						}					
				}
		}

		public override void OnInspectorGUI ()
		{
				#if UNITY_EDITOR		
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField ("Script");
				script = (Node)EditorGUILayout.ObjectField(target, typeof(Node), true);
				EditorGUILayout.EndHorizontal ();
						
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField ("Road");
				script.Road = (Road)EditorGUILayout.ObjectField(script.Road, typeof(Road), true);
				EditorGUILayout.EndHorizontal ();
				
//				EditorGUILayout.BeginHorizontal();
//				EditorGUILayout.LabelField ("Is Start For");
//				script.IsStartFor = (Trail)EditorGUILayout.ObjectField(script.IsStartFor, typeof(Trail), true);
//				EditorGUILayout.EndHorizontal ();
//				
//				EditorGUILayout.BeginHorizontal();
//				EditorGUILayout.LabelField ("Is End For");
//				script.IsEndFor = (Trail)EditorGUILayout.ObjectField(script.IsEndFor, typeof(Trail), true);
//				EditorGUILayout.EndHorizontal ();
				#endif
		}
}
