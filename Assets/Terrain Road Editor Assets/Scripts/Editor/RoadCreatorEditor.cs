﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(RoadCreator))]
public class RoadCreatorEditor : Editor
{
		RoadCreator script;
		bool isEdit = false;
		bool roadsFoldout = false;
		bool materialFoldout;

#region Viewport
		void OnSceneGUI ()
		{
				#if UNITY_EDITOR

				Event e = Event.current;
				int controlID = GUIUtility.GetControlID (FocusType.Native);
				EventType type = e.GetTypeForControl (controlID);
				
				if (type == EventType.MouseDown && isEdit) {
						switch (e.button) {						
						// Add new Road to scene
						case 0:
								GUIUtility.hotControl = controlID;
								Ray ray = HandleUtility.GUIPointToWorldRay (e.mousePosition);						
								RaycastHit hit;
								if (Physics.Raycast (ray, out hit)) {
										script.AddRoad (hit.point);
								}						
								e.Use ();
								break;
						}
				}

				if (type == EventType.KeyDown) {
						switch (e.keyCode) {
						
						// Switch between standart viewport and Road Editor controls 
						case KeyCode.Space:
								if (!isEdit) {
										GUIUtility.hotControl = controlID;
										isEdit = true;
								} else {
										GUIUtility.hotControl = 0;
										isEdit = false;
								}
							break;
						}						
						e.Use ();
				}
				#endif
		}
#endregion

#region Inspector
		public override void OnInspectorGUI ()
		{
				#if UNITY_EDITOR
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField ("Script");
				script = (RoadCreator)EditorGUILayout.ObjectField(target, typeof(RoadCreator), true);
				EditorGUILayout.EndHorizontal ();
				EditorGUILayout.Space ();
				
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField("Prefab");
				script.roadPrefab = (GameObject)EditorGUILayout.ObjectField(script.roadPrefab, typeof(GameObject), true);
				EditorGUILayout.EndHorizontal ();

				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField("Road Splat");
				script.borderSplat = EditorGUILayout.IntField(script.borderSplat);
				EditorGUILayout.EndHorizontal ();

				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField("Smooth Filter");
				script.smoothFilter = EditorGUILayout.Slider(script.smoothFilter,0f,1f);
				EditorGUILayout.EndHorizontal ();
		
				//EditorGUILayout.Space ();
				EditorGUILayout.BeginHorizontal ();
				roadsFoldout = EditorGUILayout.Foldout (roadsFoldout, "Roads");
				
				if (GUILayout.Button ("Clear Roads")) {
					script.ClearRoads();
				}
				EditorGUILayout.EndHorizontal ();
				
				EditorGUILayout.BeginVertical ();
				EditorGUILayout.Space ();
				if (roadsFoldout) {
					for (int i =0; i<script.roads.Count; i++) {
						EditorGUILayout.BeginHorizontal();
						script.roads [i] = (Road)EditorGUILayout.ObjectField (script.roads [i], typeof(Road), true);
						if(GUILayout.Button("Remove"))
						{
								script.RemoveRoad(i);
								break;
						}
						EditorGUILayout.EndHorizontal();
						EditorGUILayout.Space();
					}
				}
				EditorGUILayout.EndVertical ();
				
//				if(GUILayout.Button("Bake"))
//				{
//						script.BakeTextureFromHeightMask("Test mask", script.heightMask);					
//				}
//				if(GUILayout.Button("Bake1"))
//				{
//					script.BakeTextureFromHeightMask("Test alpha", script.alphaMask);					
//				}
				if(GUILayout.Button("Build and Paint"))
				{
					script.MakeRoads();	
					script.PaintRoads();				
				}
				if(GUILayout.Button("Plate"))
				{
					script.PlateRoads();					
				}
				if(GUILayout.Button("Smooth Heights Around"))
				{
					script.SmoothHeightsAround();
				}
				if(GUILayout.Button("Smooth Alpha Around"))
				{
					script.SmoothAlphaAround();
				}
				#endif
		}
	#endregion
}
