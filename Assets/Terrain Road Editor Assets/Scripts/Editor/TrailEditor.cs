﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Trail))]
public class TrailEditor : Editor
{
		Trail script;
		bool isEdit;

//		void OnSceneGUI ()
//		{
//				/*if (Selection.Contains (script.gameObject)) {
//						script.UnsubscribeFromNodes ();	
//				} else {
//						script.SubscribeToNodes ();
//				}*/
//
//				Event e = Event.current;
//				int controlID = GUIUtility.GetControlID (FocusType.Native);
//				EventType type = e.GetTypeForControl (controlID);
//				
//				if (type == EventType.KeyDown) {
//						switch (e.keyCode) {
//						case KeyCode.Return:
//								
//								break;
//						}
//						e.Use ();
//				}
//		}
	
		public override void OnInspectorGUI ()
		{
				#if UNITY_EDITOR

				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField ("Script");
				script = (Trail)EditorGUILayout.ObjectField(target, typeof(Trail), true);
				EditorGUILayout.EndHorizontal ();

				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField ("Start Node");
				script.StartNode = (Node)EditorGUILayout.ObjectField(script.StartNode, typeof(Node), true);
				EditorGUILayout.EndHorizontal ();

				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField ("End Node");
				script.EndNode = (Node)EditorGUILayout.ObjectField(script.EndNode, typeof(Node), true);
				EditorGUILayout.EndHorizontal ();
				#endif
		}
}
