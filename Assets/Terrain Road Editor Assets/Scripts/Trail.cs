﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
[ExecuteInEditMode]
public class Trail : MonoBehaviour
{		
		//public DelegateController.TrailMovement TrailMovement;
		[SerializeField]
		private Road
				_road;
		[SerializeField]
		private MeshFilter
				_filter;
		[SerializeField]	
		private Node
				start;
		[SerializeField]	
		private Node
				end;
		public List<GameObject> plates;

		Trail ()
		{
				plates = new List<GameObject> ();
		}

		public Road Road {
				get {
						return _road;
				}
				set {
						_road = value;
				}
		}
		
		public MeshFilter Filter {
				get {
						return _filter;
				}
				set {
						_filter = value;
				}
		}
		
		public Node StartNode {
				get { 
						return start;
				}
				set { 
						start = value;
//						start.IsStartFor = this.gameObject.GetComponent<Trail> ();
						//TrailMovement += start.OnMovement;
						
						start.NodeMovement += OnNodeMovement;

						//start.NodeMovement += NewOnNodeMovement;
				}
		}

		public Node EndNode {
				get { 
						return end;
				}
				set { 
						end = value;
//						end.IsEndFor = this.gameObject.GetComponent<Trail> ();
						//TrailMovement += end.OnMovement;
				
						end.NodeMovement += OnNodeMovement;
				
						//end.NodeMovement += NewOnNodeMovement;
				}
		}

		public void OnNodeMovement ()
		{
#if UNITY_EDITOR
				transform.position = Vector3.Lerp (start.transform.position, 
		                                   			end.transform.position, 0.5f);
				Vector3 delta = end.transform.position - start.transform.position;
				transform.localScale = new Vector3 (transform.localScale.x, 1, delta.magnitude);
				transform.forward = delta.normalized;

				transform.hasChanged = false;
#endif
		}
		
		public void SubscribeToNodes ()
		{
				start.NodeMovement += OnNodeMovement;
				end.NodeMovement += OnNodeMovement;
		}

		public void UnsubscribeFromNodes ()
		{

				start.NodeMovement -= OnNodeMovement;
				end.NodeMovement -= OnNodeMovement;
				
		}

		void OnDrawGizmos ()
		{
				Gizmos.color = Color.red;
				Gizmos.DrawSphere (transform.position, 1);
		}
}
